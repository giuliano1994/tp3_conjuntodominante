package usuario;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import codigoNegocio.ConjuntoDominante;
import codigoNegocio.Grafo;
import codigoNegocio.Solver;
import codigoNegocio.Vertice;

@SuppressWarnings("serial")
public class Ventana extends JFrame implements MouseListener {

	private JPanel contentPane;
	private Grafo grafo;
	private JComboBox<Integer> seleccionarVertice1;
	private JComboBox<Integer> seleccionarVertice2;
	private Map<Integer, Point> coordenadas;
	private ArrayList<Vertice> conjunto_dominante;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Ventana() {
		
		//Inicializador de variables
		grafo = new Grafo();
		coordenadas=new HashMap<Integer, Point>();
		conjunto_dominante=new ArrayList<Vertice>();
		contentPane = new JPanel();
		JLabel titulo = new JLabel("Conjunto Dominante Minimo");
		JTextPane instrucciones = new JTextPane();
		seleccionarVertice1 = new JComboBox<Integer>();
		seleccionarVertice2 = new JComboBox<Integer>();
		
		//Configuraciones de la ventana
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//Titulo
		titulo.setBounds(76, 30, 343, 33);
		titulo.setFont(new Font("Arial", Font.PLAIN, 24));
		contentPane.add(titulo);
		
		//Instrucciones para agregar vertices
		instrucciones.setEditable(false);
		instrucciones.setFont(new Font("Arial", Font.PLAIN, 15));
		instrucciones.setText("Para agregar un vertice haga clic en el panel de la derecha");
		instrucciones.setBounds(38, 120, 171, 74);
		instrucciones.setBackground(contentPane.getBackground());
		contentPane.add(instrucciones);
		
		//Seleccionador de vertice 1
		seleccionarVertice1.setFont(new Font("Arial", Font.PLAIN, 12));
		seleccionarVertice1.setBounds(40, 200, 75, 25);
		contentPane.add(seleccionarVertice1);
		
		//Seleccionador de vertice 2
		seleccionarVertice2.setFont(new Font("Arial", Font.PLAIN, 12));
		seleccionarVertice2.setBounds(125, 200, 75, 25);
		contentPane.add(seleccionarVertice2);
		
		//BOTONES
		JButton btnAgregarArista = new JButton("AgregarArista");
		btnAgregarArista.setFont(new Font("Arial", Font.PLAIN, 12));
		btnAgregarArista.setBounds(40, 230, 160, 25);
		contentPane.add(btnAgregarArista);
		
		JButton btnReiniciar = new JButton("Reiniciar");
		btnReiniciar.setFont(new Font("Arial", Font.PLAIN, 12));
		btnReiniciar.setBounds(275, 350, 110, 25);
		contentPane.add(btnReiniciar);
		
		JButton conj_dominante = new JButton("Conjunto Dominante");
		conj_dominante.setFont(new Font("Arial", Font.PLAIN, 12));
		conj_dominante.setBounds(40, 270, 160, 48);
		contentPane.add(conj_dominante);
		
		//Resultado escrito
		JTextPane textoResultado = new JTextPane();
		textoResultado.setEnabled(false);
		textoResultado.setEditable(false);
		
		textoResultado.setFont(new Font("Arial", Font.PLAIN, 14));
		textoResultado.setBounds(38, 344, 184, 108);
		
	
		
		contentPane.addMouseListener(this);
		
		
		//Agregar Arista
		btnAgregarArista.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(seleccionarVertice1.getSelectedItem() != null && seleccionarVertice2.getSelectedItem() != null)
				{
					try {
						grafo.agregarArista((Integer)seleccionarVertice1.getSelectedItem(),(Integer)seleccionarVertice2.getSelectedItem());
						repaint();
						}
					catch(IllegalArgumentException loop)
					{
						JOptionPane.showMessageDialog(contentPane, loop.getMessage());
					}
				}
			}
		});
		
		
		//Conjunto Dominante
		conj_dominante.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				if(grafo.tama�o() != 0)
				{
					Solver solver=new Solver(grafo);
					ConjuntoDominante solucion=solver.resolver();
					conjunto_dominante=solucion.resultado();

					String ret="";
					for(Vertice v: conjunto_dominante)
					{
						ret+= v.getNumero()+", ";
					}
					textoResultado.setText("El CONJUNTO DOMINANTE MINIMO\r\nse observa en el panel\r\ncoloreado de rojo y esta \r\nconformado por los vertices "+ret);
					contentPane.add(textoResultado);
					textoResultado.setEnabled(true);
					repaint();
				}
				else
					JOptionPane.showMessageDialog(contentPane, "Aun el grafo esta vac�o");
			}
		});
		
		
		//Reiniciar
		btnReiniciar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				grafo=new Grafo();
				seleccionarVertice1.removeAllItems();
				seleccionarVertice2.removeAllItems();
				coordenadas=new HashMap<Integer, Point>();
				conjunto_dominante=new ArrayList<Vertice>();
				contentPane.remove(textoResultado);
				repaint();
			}
		});
		
	}
	
	//Funcion que dibuja el grafo 
	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(250, 150, 200, 200);
		
		for (Integer vertice: coordenadas.keySet())
		{
			g.setColor(Color.BLACK);
			g.fillOval((int)coordenadas.get(vertice).getX(),(int)coordenadas.get(vertice).getY(), 10, 10);
			g.drawString(vertice.toString(), (int)coordenadas.get(vertice).getX(),(int)coordenadas.get(vertice).getY());
			
			Vertice v=new Vertice(vertice);
			
			for (Vertice vecino: grafo.vecinosDe(v.getNumero()))
			{
				g.drawLine((int)coordenadas.get(vertice).getX()+5, (int)coordenadas.get(vertice).getY()+5,
						(int)coordenadas.get(vecino.getNumero()).getX()+5,(int)coordenadas.get(vecino.getNumero()).getY()+5);
			}
		}
		
		for(Vertice v: conjunto_dominante)
		{
			for(Integer coord: coordenadas.keySet())
				if(v.getNumero() == coord)
				{
					g.setColor(Color.RED);
					g.fillOval((int)coordenadas.get(coord).getX(),(int)coordenadas.get(coord).getY(), 10, 10);
				}
		}
		
	}

	
	//Agregar Vertice
	@Override
	public void mouseClicked(MouseEvent e) 
	{
		if( (e.getX() > 245) && (e.getX() < 440) && (e.getY() > 123) && (e.getY() < 317) )
		{
			try
			{
				Integer vertice = Integer.parseInt(JOptionPane.showInputDialog(contentPane,"ingrese el numero para el vertice","Nombre de vertice",JOptionPane.QUESTION_MESSAGE));
				
				if(! grafo.existeVertice(vertice))
				{
					grafo.agregarVertice(vertice);
					Point coordenada=new Point(e.getX(), e.getY()+20);
					coordenadas.put(vertice, coordenada);
					seleccionarVertice1.addItem(vertice);
					seleccionarVertice2.addItem(vertice);
					repaint();
				}
			}
			catch(NumberFormatException ex)
			{
				JOptionPane.showMessageDialog(contentPane, "El valor ingresado debe numerico");
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) 
	{	
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
	}

	@Override
	public void mouseEntered(MouseEvent e) 
	{
	}

	@Override
	public void mouseExited(MouseEvent e) 
	{
	}
		
}
