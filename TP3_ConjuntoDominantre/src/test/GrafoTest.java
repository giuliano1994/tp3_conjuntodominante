package test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

import codigoNegocio.Grafo;
import codigoNegocio.Vertice;

public class GrafoTest
{

	Grafo g;
	
	@Before
	public void inicializar()
	{
		g= new Grafo();
	
		g.agregarVertice(1);
		g.agregarVertice(2);
		g.agregarVertice(3);
		g.agregarVertice(4);
		g.agregarVertice(5);
	}
	
	
	//Vertice
	@Test 
	public void agregarVerticeTest() 
	{
		g.agregarVertice(8);
		assertTrue(g.existeVertice(8));
	}
	
	
	@Test
	public void agregarVerticeDosVecesTest()
	{
		g.agregarVertice(1);
		
		assertEquals(5, g.tama�o());
	}

	
	@Test (expected = IllegalArgumentException.class)  
	public void eliminarVerticeInexistenteTest() 
	{
		g.eliminarVertice(8);
	}
	
	
	@Test
	public void eliminarVerticeTest() 
	{
		g.eliminarVertice(2);
		
		assertFalse(g.existeVertice(2));
	}

	
	@Test (expected= IllegalArgumentException.class)
	public void eliminarVerticeDosVecesTest()
	{
		g.eliminarVertice(1);
		g.eliminarVertice(1);
	}
	
	
	@Test
	public void verticeInexistenteTest()
	{
		assertFalse(g.existeVertice(7));
	}
	
	
	@Test
	public void existeVerticeTest()
	{
		assertTrue(g.existeVertice(1));
	}
	
	
	@Test
	public void getVerticesTest() 
	{
		Vertice ver1=new Vertice (1);
		Vertice ver2=new Vertice (2);
		Vertice ver3=new Vertice (3);
		Vertice ver4=new Vertice (4);
		Vertice ver5=new Vertice (5);
		
		Vertice [] esperado = {ver1,ver2,ver3,ver4,ver5};
		ArrayList<Vertice> obtenido= g.vertices();

		Assert.Iguales(esperado, obtenido);
	}
	
	
	//Arista
	@Test (expected = IllegalArgumentException.class)
	public void agregarAristaUnVerticeInexistenteTest() 
	{
		g.agregarArista(1, 7);
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarAristaVerticesInexistentesTest() 
	{
		g.agregarArista(9,8);
	}
	
	
	@Test
	public void agregarAristaDosVeces()
	{
		g.agregarArista(1, 2);
		g.agregarArista(1, 2);
		
		assertEquals(1, g.gradoDe(1));
	}
	
	
	@Test
	public void AristaInversaTest() 
	{
		g.agregarArista(1, 5);
		
		assertTrue(g.existeArista(5, 1));
	}
	
	
	@Test (expected = IllegalArgumentException.class) 
	public void agregarLoopTest()
	{
		g.agregarArista(5,5);
	}
	
	
	@Test
	public void eliminarAristaTest() 
	{
		g.agregarArista(1, 3);
		g.eliminarArista(1, 3);
		
		assertFalse(g.existeArista(1, 3));
	}
	
	
	@Test
	public void eliminarAristaInversaTest() 
	{
		g.agregarArista(1, 2);
		g.eliminarArista(2, 1);
		
		assertFalse(g.existeArista(1, 2));
	}
	
	
	@Test(expected= IllegalArgumentException.class)
	public void eliminarAristaInexistenteTest()
	{
		g.eliminarArista(1,2);
	}
	
	
	@Test(expected= IllegalArgumentException.class)
	public void eliminarAristaDosVecesTest()
	{
		g.agregarArista(1, 4);
		
		g.eliminarArista(1, 4);
		g.eliminarArista(1, 4);
	}
	
	
	@Test
	public void existeAristaTest() 
	{
		g.agregarArista(5, 3);
		
		assertEquals(1, g.gradoDe(5));
	}
	
	
	@Test
	public void aristaInexistenteTest()
	{
		assertFalse(g.existeArista(1, 2));
	}
	
	
	//Tama�o
	@Test
	public void tama�oTest() 
	{
		assertEquals(5, g.tama�o());
	}
	
	
	@Test
	public void grafoVacioTest() 
	{
		Grafo grafo = new Grafo();
		assertEquals(0, grafo.tama�o());
	}
	
	
	//Vecinos
	@Test
	public void vecinosTest() 
	{
		g.agregarVertice(7);
		g.agregarArista(7, 1);
		g.agregarArista(7, 2);
		g.agregarArista(7, 3);

		
		Vertice[] esperados = {new Vertice(1),new Vertice(2),new Vertice(3)};
		ArrayList<Vertice> obtenidos =  g.vecinosDe(7);
		Assert.Iguales(esperados, obtenidos);
	}
	
	
	@Test
	public void todosAisladosTest()
	{
		int sumaDeGrados = 0;
		for (Vertice vertice: g.vertices())
		{
			sumaDeGrados+= g.gradoDe(vertice.getNumero());
		}
		assertEquals(0,sumaDeGrados);
	}

	
	@Test
	public void verticeUniversalTest()
	{
		g.agregarVertice(7);
		g.agregarArista(7, 1);
		g.agregarArista(7, 2);
		g.agregarArista(7, 3);
		g.agregarArista(7, 4);
		g.agregarArista(7, 5);
		
		assertEquals(5, g.gradoDe(7));
	}

}
