package test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import codigoNegocio.Vertice;

public class Assert 
{

	public static void Iguales(Vertice[] esperado, ArrayList<Vertice> obtenido) 
	{
		assertEquals(esperado.length, obtenido.size());
		
		for (int i = 0; i < esperado.length; i++) 
		{
			assertTrue(obtenido.contains(esperado[i]));
		}
	}
}
