package test;

import static org.junit.Assert.*;

import org.junit.Test;

import codigoNegocio.Grafo;
import codigoNegocio.Vertice;

public class VerticeTest
{

	//agregarVecino
	@Test(expected= IllegalArgumentException.class)
	public void verificarLoopTest()
	{
		Vertice v=new Vertice(5);
		Vertice v1=new Vertice(5);
		
		v.agregarVecino(v1);	
	}
	
	
	@Test
	public void agregarVecinoDosVecesTest()
	{
		Vertice v=new Vertice(2);
		Vertice v1=new Vertice(1);
		
		v.agregarVecino(v1);
		v.agregarVecino(v1);
		
		assertEquals(1,v.grado());
	}
	
	
	@Test
	public void agregarVecinoTest()
	{
		Vertice v=new Vertice(3);
		Vertice v1=new Vertice(2);
		
		v.agregarVecino(v1);
		
		assertTrue(v.existeVecino(v1));
	}
	
	
	//eliminarVecino
	@Test(expected= IllegalArgumentException.class)
	public void vecinoInexistenteTest()
	{
		Vertice v=new Vertice(2);
		Vertice v1=new Vertice(8);
		
		v.eliminarVecino(v1);
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void eliminarVecinoDosVecesTest()
	{
		Vertice v=new Vertice(8);
		Vertice v1=new Vertice(9);
		v.agregarVecino(v1);
		
		v.eliminarVecino(v1);
		v.eliminarVecino(v1);
	}
	
	
	@Test
	public void eliminarVerticeTest()
	{
		Vertice v=new Vertice(8);
		Vertice v1=new Vertice(2);
		
		v.agregarVecino(v1);
		v.eliminarVecino(v1);
		
		assertEquals(0, v.grado());
	}
	
	
	//equals
	@Test
	public void verticeNuloTest()
	{
		Vertice v=null;
		Vertice v2=new Vertice(2);
		
		assertFalse(v2.equals(v));
	}
	
	
	@Test
	public void ObjetoDiferenteTest()
	{
		Grafo g=new Grafo();
		Vertice v=new Vertice(5);
		
		assertFalse(v.equals(g));
	}
	
	
	@Test
	public void VerticeDiferenteTest()
	{
		Vertice v=new Vertice(2);
		Vertice v1=new Vertice(3);
		
		assertFalse(v.equals(v1));
	}
	
	
	@Test 
	public void VerticeIgualesTest() 
	{
		Vertice v=new Vertice(2);
		Vertice v2=new Vertice(2);
		
		assertTrue(v.equals(v2));
	}
	
	
	//existeVecino
	@Test
	public void existeVecinoTest()
	{
		Vertice v=new Vertice(2);
		Vertice v2=new Vertice(3);
		
		v.agregarVecino(v2);
		
		assertTrue(v.existeVecino(v2));
	}
	
	
	@Test
	public void noExisteVecinoTest()
	{
		Vertice v=new Vertice(2);
		Vertice v1=new Vertice(1);
		
		assertFalse(v.existeVecino(v1));
	}
	
	
	//grado
	@Test
	public void aisladoTest()
	{
		Vertice v=new Vertice(2);
		assertEquals(0, v.grado());
	}
	
	
	@Test
	public void verticeConVecinosTest()
	{
		Vertice v=new Vertice(2);
		Vertice v1=new Vertice(1);
		Vertice v2=new Vertice(3);
		Vertice v3=new Vertice(4);
		
		v.agregarVecino(v1);
		v.agregarVecino(v2);
		v.agregarVecino(v3);
		
		assertEquals(3, v.grado());
	}
	
	
	//vecinos
	@Test
	public void vecinosTest()
	{
		Vertice v1 = new Vertice(1);
		Vertice v2 = new Vertice(2);
		Vertice v3 = new Vertice(3);
		
		v1.agregarVecino(v2);
		v1.agregarVecino(v3);
		
		Vertice[] esperados= {v2, v3};
		
		Assert.Iguales(esperados, v1.vecinos());
	}

}
