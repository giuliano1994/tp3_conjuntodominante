package test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import codigoNegocio.ConjuntoDominante;
import codigoNegocio.Grafo;
import codigoNegocio.Solver;
import codigoNegocio.Vertice;

public class SolverTest 
{

	Grafo g=new Grafo();
	
	
	@Before
	public void grafo()
	{
		g.agregarVertice(1);
		g.agregarVertice(2);
		g.agregarVertice(3);
		g.agregarVertice(4);
		g.agregarVertice(5);
		g.agregarVertice(6);
		
		g.agregarArista(1, 2);
		g.agregarArista(1, 5);
		g.agregarArista(2, 3);
		g.agregarArista(2, 5);
		g.agregarArista(3, 4);
		g.agregarArista(4, 5);
		g.agregarArista(4, 6);
	}

	
	@Test
	public void grafoConexoTest()
	{
		Solver solver=new Solver(g);
		ConjuntoDominante solucion=solver.resolver();;
		
		Vertice ver=new Vertice(2);
		Vertice ver2=new Vertice(4);
		
		Vertice [] esperado= {ver, ver2};
		ArrayList<Vertice> obtenido=solucion.resultado();
		
		Assert.Iguales(esperado, obtenido);
	}
	
	
	@Test
	public void grafoDisconexoTest()
	{
		g.eliminarArista(4, 6);
		
		Solver solver=new Solver(g);
		ConjuntoDominante solucion=solver.resolver();
		
		Vertice ver=new Vertice(2);
		Vertice ver2=new Vertice(4);
		Vertice ver3= new Vertice(6);
		
		Vertice [] esperado= {ver, ver2, ver3};
		ArrayList<Vertice> obtenido= solucion.resultado();
		
		Assert.Iguales(esperado, obtenido);
	}
	
	
	@Test
	public void todosAisladosTest()
	{
		g.eliminarArista(1, 2);
		g.eliminarArista(1, 5);
		g.eliminarArista(2, 3);
		g.eliminarArista(2, 5);
		g.eliminarArista(3, 4);
		g.eliminarArista(4, 5);
		g.eliminarArista(4, 6);
		
		Solver solver=new Solver(g);
		ConjuntoDominante solucion=solver.resolver();
		
		Vertice ver1=new Vertice(1);
		Vertice ver2=new Vertice(2);
		Vertice ver3=new Vertice(3);
		Vertice ver4=new Vertice(4);
		Vertice ver5=new Vertice(5);
		Vertice ver6=new Vertice(6);
		
		Vertice [] esperados= {ver1, ver2, ver3, ver4, ver5, ver6};
		ArrayList<Vertice> obtenidos=solucion.resultado();
		
		Assert.Iguales(esperados, obtenidos);
	}
	
	
	@Test
	public void unVerticeTest()
	{
		Grafo grafo=new Grafo();
		
		grafo.agregarVertice(1);
		
		Solver solver=new Solver(grafo);
		ConjuntoDominante solucion= solver.resolver();
		
		Vertice [] esperado= {new Vertice(1)};
		ArrayList<Vertice> obtenido= solucion.resultado();
		
		Assert.Iguales(esperado, obtenido);
	}
	
	
	@Test
	public void grafoVacioTest()
	{
		Grafo grafo2 = new Grafo();
		
		Solver solver=new Solver(grafo2);
		ConjuntoDominante solucion= solver.resolver();
		
		assertEquals(0,solucion.tamaņoSolucion());
	}
	
}
