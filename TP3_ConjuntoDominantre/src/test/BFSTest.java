package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import codigoNegocio.BFS;
import codigoNegocio.Grafo;
import codigoNegocio.Vertice;

public class BFSTest {
	
	Grafo grafo;

	@Before
	public void iniciarGrafo()
	{
		grafo = new Grafo();
		
		grafo.agregarVertice(1);
		grafo.agregarVertice(2);
		grafo.agregarVertice(3);
		grafo.agregarVertice(4);
		grafo.agregarVertice(5);
	}

	
	
	@Test (expected=NullPointerException.class)
	public void grafoNuloTest() 
	{
		Grafo g = null;
		
		assertFalse(BFS.esConexo(g));
	}
	
	
	
	@Test
	public void esConexoTest() 
	{
		grafo.agregarArista(1, 3);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(3, 4);
		grafo.agregarArista(3, 2);
		grafo.agregarArista(2, 5);
		
		assertTrue(BFS.esConexo(grafo));
	}
	
	
	@Test
	public void noConexoTest() 
	{
		grafo.agregarArista(4, 5);
		grafo.agregarArista(1, 3);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(3, 2);
		
		assertFalse(BFS.esConexo(grafo));
	}
	
	
	
	@Test 
	public void grafoVacioTest() 
	{
		Grafo g = new Grafo();
		
		assertTrue(BFS.esConexo(g));
	}
	
	
	@Test
	public void alcanzablesTest()
	{
		grafo.agregarArista(1, 3);
		grafo.agregarArista(3, 2);
		grafo.agregarArista(5, 2);
		grafo.agregarArista(5, 4);
		
		ArrayList<Vertice> obtenidos=BFS.alcanzables(grafo, new Vertice(1));
		Vertice[] esperados = {new Vertice(1), new Vertice(2), new Vertice(3), new Vertice(4), new Vertice(5)};
		
		Assert.Iguales(esperados,obtenidos);
	}
	
	
	@Test
	public void unaSolaAristaTest()
	{
		Grafo g=new Grafo();
		g.agregarVertice(2);
		g.agregarVertice(1);
		g.agregarArista(1, 2);
		assertTrue(BFS.esConexo(g));
	}
	
}
