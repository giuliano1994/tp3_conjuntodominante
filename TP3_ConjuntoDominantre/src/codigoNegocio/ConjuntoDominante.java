package codigoNegocio;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ConjuntoDominante
{
	private ArrayList<Vertice> solucion;
	private Set <Vertice> alcanzados;
	
	
	
	public ConjuntoDominante()
	{
		this.solucion= new ArrayList <Vertice>();
		this.alcanzados= new HashSet<Vertice>();
	}
	
	
	//Agregar los vertices que aun no estan dominados
	public void agregarNoDominado(Vertice vertice)
	{
		if(! alcanzados.contains(vertice))
		{
			this.solucion.add(vertice);
			this.alcanzados.add(vertice);
			for (Vertice vecino: vertice.vecinos() ) 
			{
				if(! alcanzados.contains(vecino))
				{
					alcanzados.add(vecino);
				}
			}
		}
	}
		
	
	//Agrega los vertices que fueron ordenados por grado previamente
	public void agregarVerticeMayorGrado(Vertice vertice)
	{
		{	
			this.solucion.add(vertice);
			this.alcanzados.add(vertice);
			for (Vertice vecino : vertice.vecinos()) 
			{
				if(! alcanzados.contains(vecino))
				{
					this.alcanzados.add(vecino);
				}
			}
		}
	}
		
	
	//Devuelve una copia del conjunto solucion
	@SuppressWarnings("unchecked")
	public ArrayList<Vertice> resultado()
	{
		return (ArrayList<Vertice>) this.solucion.clone();
	}
	
	
	//devuelve el tama�o del conjunto solucion
	public int tama�oSolucion()
	{
		return this.solucion.size();
	}
	
	
	//Devuelve el tama�o del conjunto de vertices alcanzados
	public int cantidadVerticesAlcanzados()
	{
		return this.alcanzados.size();
	}
}
