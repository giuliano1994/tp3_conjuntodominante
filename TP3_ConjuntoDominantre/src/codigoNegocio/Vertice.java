package codigoNegocio;
import java.util.ArrayList;

public class Vertice 
{
	private int numero;
	private ArrayList<Vertice> vecinos;
	

	public Vertice(Integer numero)
	{
		this.numero=numero;
		this.vecinos=new ArrayList<Vertice>();
	}
	
	
	//Agrega el vertice ppasado como parametro a su lista de vecinos
	public void agregarVecino(Vertice vertice)
	{
		verificarLoop(vertice);
		
		if(! existeVecino(vertice))
			vecinos.add(vertice);
	}
	
	
	//Elimina de su lista de vecinos el vertice pasado como parametro 
	public void eliminarVecino(Vertice vertice)
	{
		if(existeVecino(vertice))
			vecinos.remove(vertice);
		else
			throw new IllegalArgumentException("La arista entre " + getNumero()+ " y "+vertice.getNumero()+" no existe");
	}
	
	
	//Devuelve el numero del vertice
	public  int getNumero()
	{
		return this.numero;
	}

	
	//Devuelve true si el vertice pasado como parametro existe entre la lista de vecinos
	public boolean existeVecino(Vertice vertice)
	{
		return vecinos.contains(vertice);
	}
	
	
	//Si se intenta agregar una arista entre el mismo vertice devuelve una excepcion
	private void verificarLoop(Vertice vertice)
	{
		if(this.numero == vertice.getNumero())
		{
			throw new IllegalArgumentException("El v�rtice "+ this.numero+ " no puede crear una arista con s� mismo");
		}
	}
	
	
	//Devuelve la cantidad de vecinos del vertice
	public int grado()
	{
		return this.vecinos.size();
	}
	
	
	//Devuelve una copia del conjunto de vecinos del vertice
	@SuppressWarnings("unchecked")
	public ArrayList <Vertice> vecinos()
	{
		return (ArrayList<Vertice>) this.vecinos.clone();
	}
	
	
	//Devuelve true si el objeto pasado como par�metro es igual al vertice
	@Override
	public boolean equals(Object o)
	{
		if(o == null)
		{
			return false;
		}
		if(this.getClass() != o.getClass())
		{
			return false;
		}
		
		Vertice vertice=(Vertice) o;
		
		return this.numero == vertice.getNumero();
		
	}
	
}
