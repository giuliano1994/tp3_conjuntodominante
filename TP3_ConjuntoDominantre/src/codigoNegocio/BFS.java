package codigoNegocio;
import java.util.ArrayList;

public class BFS 
{
	private static ArrayList<Vertice> marcados;
	private static ArrayList<Vertice> pendientes;


	//verifica si un garfo es conexo
	public static boolean esConexo(Grafo grafo)
	{
		if(grafo == null)
		{
			throw new NullPointerException("el grafo es nulo");
		}
		if(grafo.tama�o() == 0)
		{
			return true;
		}
		return alcanzables(grafo, grafo.vertices().iterator().next()).size() == grafo.tama�o();
	}


	//devuelve un conjunto con los vertices a los que puede llegar desde un vertice de origen
	public static ArrayList<Vertice> alcanzables(Grafo grafo, Vertice vertice) 
	{
		inicializar(grafo, vertice);
		while (pendientes.size() > 0)
		{
			Vertice verticePendiente=pendientes.get(0);
			if(! marcados.contains(verticePendiente))
			{
				marcados.add(verticePendiente);
			}
			agregarVecinosPendientes(grafo, verticePendiente);
			pendientes.remove(0);
		}
		return marcados;
	}



	//agrega en la lista de pendientes los vecinos de un vertice que aun no fueron alcanzados
	private static void agregarVecinosPendientes(Grafo grafo, Vertice vertice) 
	{
		for (Vertice vecino : grafo.vecinosDe(vertice.getNumero())) 
		{
			if(marcados.contains(vecino) == false && pendientes.contains(vecino) == false)
			{
				pendientes.add(vecino);
			}
		}
	}



	//inicializa el recorrido por el grafo
	static private void inicializar(Grafo g, Vertice vertice)
	{
		pendientes = new ArrayList<Vertice>();
		pendientes.add(vertice);
		marcados = new ArrayList<Vertice>();
	}

}


