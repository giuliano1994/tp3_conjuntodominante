package codigoNegocio;
import java.util.ArrayList;

public class Grafo 
{
	private ArrayList<Vertice> grafo;

	
	public Grafo()
	{
		this.grafo= new ArrayList<Vertice>();
	}

	
	//Agrega un vertice al grafo
	public void agregarVertice(Integer numero)
	{
		Vertice vertice = new Vertice(numero);
		if(!existeVertice(numero))
		{
			this.grafo.add(vertice);
		}
	}

	
	//Agrega una arista al grafo
	public void agregarArista(Integer vertice1, Integer vertice2)
	{
		if(existeVertice(vertice1) && existeVertice(vertice2))
		{
			Vertice v1=getVertice(vertice1);
			Vertice v2=getVertice(vertice2);
			v1.agregarVecino(v2);
			v2.agregarVecino(v1);
		}
		else
		{
			throw new IllegalArgumentException("La arista entre "+ vertice1+" y "+ vertice2 + " no existe");
		}
	}

	
	//Devuelve true si existe la arista entre los vertices consultados, false en caso contrario
	public boolean existeArista(Integer vertice1, Integer vertice2)
	{
		
		return existeVertice(vertice1) && getVertice(vertice1).existeVecino(new Vertice(vertice2));
	}

	
	//Devuelve true si existe el vertice consultado o false en caso contrario
	public boolean existeVertice(Integer vertice)
	{
		Vertice v=getVertice(vertice);
		return v != null ? true : false;
	}

	
	//Elimina la arista entre los vertices pasados como parametro
	public void eliminarArista(Integer vertice1, Integer vertice2) 
	{
		if(existeVertice(vertice1) && existeVertice(vertice2))
		{
			Vertice v=getVertice(vertice1);
			Vertice v2=getVertice(vertice2);
			v.eliminarVecino(v2);
			v2.eliminarVecino(v);
		}
	}
	
	
	//Elimina el vertice pasado como parametro (si existe)
	public void eliminarVertice(Integer vertice)
	{
		if(existeVertice(vertice)) 
		{
			Vertice v=getVertice(vertice);
			for (Vertice vecino : v.vecinos()) 
			{
				vecino.eliminarVecino(v);
			}

			this.grafo.remove(v);
		}
		else
			throw new IllegalArgumentException("El v�rtice " + vertice+ " no existe");
	}
	
	
	//Devuelve la cantidad de vertices que contiene el grafo
	public int tama�o()
	{
		return this.grafo.size();
	}
	
	
	//Devuelve el conjunto de vecinos de el vertice pasado como parametro
	public ArrayList<Vertice> vecinosDe(Integer vertice)
	{
		ArrayList<Vertice> vecinos=new ArrayList<Vertice>();
		if(existeVertice(vertice))
		{
			vecinos=getVertice(vertice).vecinos();
			return vecinos;
		}
		else
			throw new IllegalArgumentException("El v�rtice "+ vertice+" no existe");
	}
	
	
	//Devuelve una copia del conjunto de vertices que conforman el grafo
	@SuppressWarnings("unchecked")
	public ArrayList<Vertice> vertices()
	{
		return (ArrayList<Vertice>)this.grafo.clone();
	}

	
	//Devuelve la cantidad de vecinos que tiene el vertice pasado como parametro
	public int gradoDe(Integer vertice)
	{
		int grado=0;
		if(existeVertice(vertice))
		{
			grado=getVertice(vertice).grado();
		}
		return grado;
	}
	
	
	//Devuelve la referencia a un vertice
	private Vertice getVertice(Integer numero)
	{
		Vertice vertice=null;
		
		for(Vertice v: vertices())
		{
			if(v.getNumero() == numero)
			{
				vertice=v;
			}
		}
		return vertice;
	}

}
