package codigoNegocio;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Solver 
{
	private Grafo instancia;
	
	
	public Solver(Grafo grafo)
	{
		this.instancia=grafo;
	}
	
	
	//Devuelve un Objeto de tipo ConjuntoDominante con una posible solucion
	public ConjuntoDominante resolver()
	{
		ConjuntoDominante ret=new ConjuntoDominante();
		
		if(BFS.esConexo(instancia))
		{
			ret=esConexo();
		}
		else
		{
			ret= noEsConexo();
		}
		
		return ret;
	}

		
	//Resuelve el problema del Conjunto dominante cuando la instancia es disconexa
	private ConjuntoDominante noEsConexo() 
	{
		ConjuntoDominante ret=new ConjuntoDominante();
		
		for(Vertice vertice: verticesOrdenados())
		{
			if(ret.cantidadVerticesAlcanzados() < this.instancia.tama�o())
			{
				ret.agregarNoDominado(vertice);
			}
		}
		return ret;
	}
	
	
	//Resuelve el problema del Conjunto dominante cuando la instancia es conexa
	private ConjuntoDominante esConexo()
	{
		ConjuntoDominante ret=new ConjuntoDominante();
		
		for(Vertice vertice: verticesOrdenados())
		{
			if(ret.cantidadVerticesAlcanzados() < this.instancia.tama�o())
			{
				ret.agregarVerticeMayorGrado(vertice);
			}
		}
		return ret;
	}	
	
	
	//Devuelve un conjunto con los vertices del grafo ordenados por grado
	private ArrayList<Vertice> verticesOrdenados()
	{
		ArrayList<Vertice> ret= instancia.vertices();
		
		Collections.sort(ret, new Comparator<Vertice>()
		{

			@Override
			public int compare(Vertice vertice1, Vertice vertice2) 
			{
				if(vertice1.grado()==0)
				{
					return -1;
				}
				if(vertice1.grado() > vertice2.grado())
				{
					return -1;
				}
				else if(vertice1.grado() < vertice2.grado())
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
		});
		return ret;
	}
	
}
